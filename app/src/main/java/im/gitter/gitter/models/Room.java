package im.gitter.gitter.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Room implements RoomListItem {

    public static final String _ID = "_id";
    public static final String NAME = "name";
    public static final String URI = "uri";
    public static final String URL = "url";
    public static final String AVATAR_URL = "avatarUrl";
    public static final String TOPIC = "topic";
    public static final String ONE_TO_ONE = "oneToOne";
    public static final String ACTIVITY = "activity";
    public static final String LURK = "lurk";
    public static final String UNREAD_ITEMS = "unreadItems";
    public static final String MENTIONS = "mentions";
    public static final String ROOM_MEMBER = "roomMember";
    public static final String LAST_ACCESS_TIME = "lastAccessTime";
    public static final String USER_ID = "userId";
    public static final String GROUP_ID = "groupId";

    @NonNull private final String id;
    @NonNull private final String name;
    @NonNull private final String uri;
    @NonNull private final String url;
    @NonNull private final String avatarUrl;
    @Nullable private final String topic;
    private final boolean oneToOne;
    private final boolean activity;
    private final boolean lurk;
    private final int unreadItems;
    private final int mentions;
    private final boolean roomMember;
    @Nullable private final Long lastAccessTime;
    @Nullable private final String userId;
    @Nullable private final String groupId;

    public Room(
            @NonNull String id,
            @NonNull String name,
            @NonNull String uri,
            @NonNull String url,
            @NonNull String avatarUrl,
            @Nullable String topic,
            boolean oneToOne,
            boolean activity,
            boolean lurk,
            int unreadItems,
            int mentions,
            boolean roomMember,
            @Nullable Long lastAccessTime,
            @Nullable String userId,
            @Nullable String groupId
    ) {
        this.id = id;
        this.name = name;
        this.uri = uri;
        this.url = url;
        this.avatarUrl = avatarUrl;
        this.topic = topic;
        this.oneToOne = oneToOne;
        this.activity = activity;
        this.lurk = lurk;
        this.unreadItems = unreadItems;
        this.mentions = mentions;
        this.roomMember = roomMember;
        this.lastAccessTime = lastAccessTime;
        this.userId = userId;
        this.groupId = groupId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean hasActivity() {
        return (lurk && activity) || unreadItems > 0 || mentions > 0;
    }

    @Override
    public int getUnreadCount() {
        return unreadItems;
    }

    @Override
    public int getMentionCount() {
        return mentions;
    }

    @Override
    public String getRoomId() {
        return getId();
    }

    public String getUri() {
        return url.substring(1);
    }

    @Override
    public String getAvatarUrl(int size) {
        return avatarUrl + "?s=" + size;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public int getUnreadItems() {
        return unreadItems;
    }

    public int getMentions() {
        return mentions;
    }

    public boolean isOneToOne() {
        return oneToOne;
    }

    public boolean isRoomMember() {
        return roomMember;
    }

    public static Room newInstance(Cursor cursor) {
        ContentValues contentValues = new ContentValues();
        DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
        return Room.newInstance(contentValues);
    }

    public static Room newInstance(ContentValues contentValues) {
        return new Room(
                contentValues.getAsString(_ID),
                contentValues.getAsString(NAME),
                contentValues.getAsString(URI),
                contentValues.getAsString(URL),
                contentValues.getAsString(AVATAR_URL),
                contentValues.getAsString(TOPIC),
                contentValues.getAsInteger(ONE_TO_ONE) != 0,
                contentValues.getAsInteger(ACTIVITY) != 0,
                contentValues.getAsInteger(LURK) != 0,
                contentValues.getAsInteger(UNREAD_ITEMS),
                contentValues.getAsInteger(MENTIONS),
                contentValues.getAsInteger(ROOM_MEMBER) != 0,
                contentValues.getAsLong(LAST_ACCESS_TIME),
                contentValues.getAsString(USER_ID),
                contentValues.getAsString(GROUP_ID)
        );
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(_ID, id);
        contentValues.put(NAME, name);
        contentValues.put(URI, uri);
        contentValues.put(URL, url);
        contentValues.put(AVATAR_URL, avatarUrl);
        contentValues.put(TOPIC, topic);
        contentValues.put(ONE_TO_ONE, oneToOne);
        contentValues.put(ACTIVITY, activity);
        contentValues.put(LURK, lurk);
        contentValues.put(UNREAD_ITEMS, unreadItems);
        contentValues.put(MENTIONS, mentions);
        contentValues.put(ROOM_MEMBER, roomMember);
        contentValues.put(LAST_ACCESS_TIME, lastAccessTime);
        contentValues.put(USER_ID, userId);
        contentValues.put(GROUP_ID, groupId);
        return contentValues;
    }
}
