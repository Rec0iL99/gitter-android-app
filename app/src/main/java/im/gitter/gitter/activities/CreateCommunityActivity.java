package im.gitter.gitter.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.HashMap;
import java.util.Map;

import im.gitter.gitter.LoginData;
import im.gitter.gitter.R;

public class CreateCommunityActivity extends AppCompatActivity {

    public static final String RESULT_EXTRA_ROOM_URI = "roomUri";
    private static final String PERMANENT_ROOM = "gitterHQ/gitter";
    private WebView webView;
    private CircularProgressView loadingSpinner;
    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);

            if (!uri.getHost().equals("gitter.im")) {
                // trying to load a different domain. no thanks.
                return true;
            }

            String roomUri = uri.getPath().substring(1);

            if (roomUri.equals(PERMANENT_ROOM)) {
                return false;
            } else {
                // room has been created, so navigate to it!
                Intent intent = new Intent();
                intent.putExtra(RESULT_EXTRA_ROOM_URI, roomUri);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            webView.evaluateJavascript(
                    "var style = document.createElement('style');" +
                    "style.type = 'text/css';" +
                    "style.innerHTML = '.community-create-close-button { display: none; }';" +
                    "document.getElementsByTagName('head')[0].appendChild(style);", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        webView.setVisibility(View.VISIBLE);
                        loadingSpinner.setVisibility(View.GONE);
                    }
                });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Create Community");

        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        loadingSpinner = (CircularProgressView) findViewById(R.id.loading_spinner);

        CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        webView = (WebView) findViewById(R.id.webv);
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.setWebViewClient(webViewClient);

        webView.setVisibility(View.INVISIBLE);
        Map<String, String> addtionalHeaders = new HashMap<>();
        addtionalHeaders.put("Authorization", "Bearer " + new LoginData(this).getAccessToken());
        webView.loadUrl("https://gitter.im/" + PERMANENT_ROOM + "#createcommunity", addtionalHeaders);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
